﻿using System;
using Microsoft.VisualBasic;
using System.Collections.Generic;
namespace practice
{
    public static class StringSolution
    {
        // Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
        public static int FindString(string haystack, string needle)
        {
            // pointer for needle
            int needlePointer = 0;
            // loop through haystack
            for (int i = 0; i < haystack.Length; i++)
            {
                // case where condition is completed
                if (needlePointer == needle.Length)
                {
                    return i - needlePointer;
                }
                // case where conditions are satisfied
                if (haystack[i] == needle[needlePointer])
                {
                    needlePointer++;
                    continue;
                }
                needlePointer = 0;
            }
            return -1;
            // Or just use haystack.IndexOf(needle); 
        }
    }
}

