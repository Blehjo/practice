﻿using System;
namespace practice;

public class Solution
{
    public static int RemoveElement(int[] nums, int val)
    {
        int currentSpot = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] != val)
            {
                nums[currentSpot] = nums[i];
                currentSpot++;
            }
        }
        return currentSpot;
    }
}

