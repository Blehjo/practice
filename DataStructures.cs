﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace practice;

public class DataStructures
{
    // Types of data structures and how to instantiate them
    /*
        Single Dimensional Arrays
        int[] numbers = neew int[6]{1,2,3,4,5,6}; or
        int[] numbers = new int[]{1,2,3,4,5,6}; or
        int[] numbers = {1,2,3,4,5,6};        
    */
    public static void ArrayEx()
    {
        ArrayExample.ArrayEx(1);
    }

    /*
        The Multidimensional Array can be declared by adding commas in the square brackets
        Ex. [,] or [,,]
        int[,] multiArr = new int[3,2] {{1,1}, {2,2}, {3,3}};
        - First integer defines rows and second integer defines columns
    */
    public static void MultiArrEx()
    {
        MultiArrExample.MultiArrEx();
    }

    /*
        A Jagged Array is an array of array. Jagged arrays store arrays instead of literal values
        - A jagged array is initialized with two square brackets [][]
        - The first bracket specifies the size of an array
        - The second bracket specifies the dimensions of the array which is going to be stored
        - int[][] jArray1 = new int[2][]; // can include two single-dimensional arrays 
        - int[][,] jArray2 = new int[3][,]; // can include three two-dimensional arrays 
    */
    public static void JaggedArrEx()
    {
        JaggedArrExample.JaggedArrEx();
    }

    /*
        An Indexer is a special type of property that allows a class or a structure to be accessed like an array for its internal collection 
    */
    public static StringDataStore IndexerEx()
    {
        StringDataStore stringData = new StringDataStore();
        stringData[0] = "hello";
        stringData[1] = "world";
        Console.WriteLine(stringData[0]);
        Console.WriteLine(stringData[1]);
        return stringData;
    }

    /*
        In C#, generic means not specific to a particular data type
        C# allows you to define generic classes, interfaces, abstract classes, fields, methods, static methods, properties, events, delegates,
        and operators using the type parameter and without the specific data type
        
    */
    public static void GenericExample()
    {
        Datastore<string, int> datastore = new Datastore<string, int>();
        datastore.Key = "Consoles";
        datastore.Value = 1;
        Console.Write(datastore.Key + ": ");
        Console.Write(datastore.Value);
    }

    /*
        In C#, the ArrayList is a non-generic collection of objects whose size increases dynamically. It is the same as Array except that its size increases dynamically.

        An ArrayList can be used to add unknown data where you don't know the types and the size of the data.
    */
    public static void ArrListEx()
    {
        ArrList.ArrListEx();
    }

    public static void ListEx()
    {
        ListExample.ListEx();
    }

    public static void LinkedListEx()
    {
        LinkedListExample.LinkedListEx();
    }

    public static void SortedListEx()
    {
        SortedListExample.SortedEx();
    }

    public static void DictionaryEx()
    {
        DictionaryExample.DictionaryEx();
    }

    public static void SortedDictionaryEx()
    {
        SortedDictionaryExample.SortedDictionaryEx();
    }

    public static void HashtableEx()
    {
        HashtableExample.HashtableEx();
    }

    public static void HashSetEx()
    {
        HashSetExample.HashSetEx();
    }

    public static void StackEx()
    {
        StackExample.StackEx();
    }

    public static void QueueEx()
    {
        QueueExample.QueueEx();
    }

    public static void DelegateEx()
    {
        DelegateExample.DelegateEx1();
    }

    public static void FuncEx()
    {
        FuncExample.FuncEx();
    }

    public static void ActionEx()
    {
        ActionExample.DoNothing();
    }
}

// Single-Dimensional Array
public class ArrayExample
{
    public static int ArrayEx(int val)
    {
        int[] numbers = new int[3] { 1, 2, 3 };
        int pointer = 0;
        foreach(int number in numbers)
        {
            if (number == val)
            {
                pointer++;
            }
        }
        return pointer;
    }
}

// Multi-Dimensional Array
public class MultiArrExample
{
    public static int MultiArrEx()
    {
        int[,,] orders = new int[1, 2, 3]
        {
            { {1,1,1}, {2,2,2} }
        };

        foreach (int number in orders)
        {
            Console.WriteLine(number);
        }
        return -1;
    }
}

// Jagged Array
public class JaggedArrExample
{
    public static void JaggedArrEx()
    {
        int[][,] orders = new int[2][,];
        orders[0] = new int[1, 2] { { 1, 1 } };
        orders[1] = new int[2, 2] { {2, 2}, { 2, 2 } };
        for(int i = 0; i < orders.Length; i++)
        {
            for(int j = 0; j < (orders[i]).Length; j++)
            {
                //Console.WriteLine(orders[i][j]);
            }
        }
    }
}

// Indexer
public class StringDataStore
{
    private string[] strArr = new string[10]; // internal data storage

    public string this[int index]
    {
        get => strArr[index];

        set => strArr[index] = value;
    }
}

// Generics
public class Datastore<TKey, TValue>
{
    public TKey Key { get; set; }
    public TValue Value { get; set; }   
}

// ArrayList Non-Generic
public class ArrList
{
    public static void ArrListEx()
    {
        int[] arr = new int[3] {1, 1, 1};
        Queue queue = new Queue();
        queue.Enqueue("hello");
        queue.Enqueue("world");
        var arrEx = new ArrayList
        {
            "Hello",
            1
        };
        arrEx.AddRange(arr);
        arrEx.AddRange(queue);
        foreach (var element in arrEx)
        {
            Console.WriteLine(element);
        }
    }
}

// List Generic (Generic of ArrayList. Has same methods)
internal class ListExample
{
    public static void ListEx()
    {
        IList<int> list = new List<int>(new int[] {1, 2, 3 });
        foreach(int number in list)
        {
            Console.WriteLine(number);
        }
    }
}

// SortedList (Generic and Non-Generic Collection)
internal class SortedListExample
{
    public static void SortedEx()
    {
        SortedList<int, string> sortedList = new SortedList<int, string>(){ { 5, "E" }, { 2, "H" }, { 9, "L" }, { 10, "L" }, { 20, "O" } };
        foreach(KeyValuePair<int, string> keyValuePair in sortedList)
        {
            Console.WriteLine("{0}: {1}", keyValuePair.Key, keyValuePair.Value);
        }
    }
}

// Dictionary (Only Generic)
internal class DictionaryExample
{
    public static void DictionaryEx()
    {
        IDictionary<int, string> dictionary = new Dictionary<int, string>()
        {
            { 5, "E" }, { 2, "H" }, { 9, "L" }, { 10, "L" }, { 20, "O" }
        };

        foreach(KeyValuePair<int, string> de in dictionary)
        {
            Console.WriteLine("{0}: {1}", de.Key, de.Value);
        }
    }
}

internal class LinkedListExample
{
    public static void LinkedListEx()
    {
        LinkedList<string> linkedlist = new LinkedList<string>();
        linkedlist.AddFirst("Hello");
        linkedlist.AddAfter(linkedlist.First, "World");
        linkedlist.AddLast("Goodbye");
        linkedlist.AddLast("Friend");
        //foreach(LinkedListNode<string> node in linkedlist)
        //{
        //    Console.WriteLine("{0}: {1}", linkedlist.Find(node), node);
        //}
    }
}

// Sorted Dictionary
internal class SortedDictionaryExample
{
    public static void SortedDictionaryEx()
    {
        SortedDictionary<int, string> sortedDictionary = new SortedDictionary<int, string>()
        {
            { 5, "E" }, { 2, "H" }, { 9, "L" }, { 10, "L" }, { 20, "O" }
        };

        foreach(KeyValuePair<int, string> keyValuePair in sortedDictionary)
        {
            Console.WriteLine("{0}: {1}", keyValuePair.Key, keyValuePair.Value);
        }
    }
}

// Hashtable (Non-Generic)
internal class HashtableExample
{
    public static void HashtableEx()
    {
        Hashtable hashtable = new Hashtable()
        {
            {"1", "H" }, {2, "E"}, {"L", 3}, {4, "L"}, {5, 0}
        };

        foreach(DictionaryEntry dictionaryEntry in hashtable)
        {
            Console.WriteLine("{0}: {1}", dictionaryEntry.Key, dictionaryEntry.Value);
        }
    }
}

// Hashset
internal class HashSetExample
{
    public static void HashSetEx()
    {
        HashSet<int> hashset = new HashSet<int>()
        {
            { 1 }, { 2 }, { 3 }, { 4 }, { -1 }, { -5 }
        };
        foreach(int number in hashset)
        {
            Console.WriteLine(number);
        }
    }
}

// Stack (Generic and Non-Generic Collection)
public class StackExample
{
    public static void StackEx()
    {
        Stack<int> stack = new Stack<int>(new int[] {1,2,3});
        foreach(int number in stack)
        {
            Console.Write("Stack...");
            Console.WriteLine(number);
        }
    }
}

// Queue (Generic and Non-generic Collection)
public class QueueExample
{
    public static void QueueEx()
    {
        Queue<int> queue = new Queue<int>(new int[] { 1, 2, 3 });
        foreach (int number in queue)
        {
            Console.Write("Queue...");
            Console.WriteLine(number);
        }
    }
}

// Tuple
public class TupleExample
{
    public static void TupleEx()
    {
        Tuple<int, char, string> tuple = new Tuple<int, char, string>(1,'a', "bleh");
        Console.WriteLine(tuple);
    }

    public static void TupleEx2()
    {
        (int, char, string) tuple = new (2, 'b', "seton");
        Console.WriteLine(tuple);
    }

    public static void TupleEx2((int, char, string) student)
    {
        Console.WriteLine(student);
    }
}

// Delegate
public class DelegateExample
{
    public delegate double DelegatedEx(double number);

    public static void DelegateEx1()
    {
        DelegatedEx delegatedEx = (double number) => Math.Pow(number,number);
        Console.WriteLine(delegatedEx.Invoke(2));
    }

    public static double DelegateEx(double number)
    {
        double result = Math.Pow(number, number);
        return result;
    }
}

// Func Delegate
public class FuncExample
{
    public static void FuncEx()
    {
        Func<int, int, int> doubleUp = DoubleUp;
        Console.WriteLine(doubleUp(20,15));
    }

    public static int DoubleUp(int firstNumber, int secondNumber)
    {
        return firstNumber * secondNumber;
    }
}

// Action Delegate
public class ActionExample
{
    public static void ActionEx()
    {
        Action action = DoNothing;
        action();
    }

    public static void DoNothing()
    {
        Console.WriteLine("Nothing Really...");
    }
}