﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;

//namespace Solution
//{

//    public class Team
//    {
//        public string teamName;
//        public int noOfPlayers;
//        public Team(string _teamName, int _noOfPlayers)
//        {
//            teamName = _teamName;
//            noOfPlayers = _noOfPlayers;
//        }

//        public void AddPlayer(int count)
//        {
//            noOfPlayers += count;
//        }

//        public bool RemovePlayer(int count)
//        {
//            if (noOfPlayers > count)
//            {
//                noOfPlayers -= count;
//                return true;
//            }
//            return false;
//        }
//    }

//    public class Subteam : Team
//    {
//        public Subteam(string teamName, int noOfPlayers) : base(teamName, noOfPlayers)
//        {
//        }

//        public void ChangeTeamName(string name)
//        {
//            teamName = name;
//        }
//    }

//    class Solution
//    {
//        static void Main(string[] args)
//        {

//            if (!typeof(Subteam).IsSubclassOf(typeof(Team)))
//            {
//                throw new Exception("Subteam class should inherit from Team class");
//            }

//            String str = Console.ReadLine();
//            String[] strArr = str.Split();
//            string initialName = strArr[0];
//            int count = Convert.ToInt32(strArr[1]);
//            Subteam teamObj = new Subteam(initialName, count);
//            Console.WriteLine("Team " + teamObj.teamName + " created");

//            str = Console.ReadLine();
//            count = Convert.ToInt32(str);
//            Console.WriteLine("Current number of players in team " + teamObj.teamName + " is " + teamObj.noOfPlayers);
//            teamObj.AddPlayer(count);
//            Console.WriteLine("New number of players in team " + teamObj.teamName + " is " + teamObj.noOfPlayers);


//            str = Console.ReadLine();
//            count = Convert.ToInt32(str);
//            Console.WriteLine("Current number of players in team " + teamObj.teamName + " is " + teamObj.noOfPlayers);
//            var res = teamObj.RemovePlayer(count);
//            if (res)
//            {
//                Console.WriteLine("New number of players in team " + teamObj.teamName + " is " + teamObj.noOfPlayers);
//            }
//            else
//            {
//                Console.WriteLine("Number of players in team " + teamObj.teamName + " remains same");
//            }

//            str = Console.ReadLine();
//            teamObj.ChangeTeamName(str);
//            Console.WriteLine("Team name of team " + initialName + " changed to " + teamObj.teamName);
//        }
//    }
//}

//using System;
//using System.Collections.Generic;
//using System.IO;

//namespace Solution
//{

//    public class NotesStore
//    {
//        public List<string> notes;

//        public NotesStore()
//        {
//            notes = new List<string>();
//        }

//        public void AddNote(String state, String name)
//        {
//            // Create a collection?
//            if (name.Length == 0)
//            {
//                ArgumentException exception = new ArgumentException("Name cannot be empty");
//            }
//            // If it's not 'completed', 'active', or 'others'
//            switch (state)
//            {
//                case 'completed':
//                    notes.Add($"{state}, {name}");
//                    break;
//                case 'active':
//                    notes.Add($"{state}, {name}");
//                    break;
//                case 'others':
//                    notes.Add($"{state}, {name}");
//                    break;
//                default:
//                    throw new Exception($"Invalid state {state}");
//                    break;
//            }
//        }
//        public List<String> GetNotes(String state)
//        {
//            IList<string> filteredNotes = new List();
//            foreach (string note in notes)
//            {
//                if (note.Contains(state))
//                {

//                }
//                throw new Exception($"Invalid state {state}");
//                string foundNote =
//                filteredNotes
//            }
//            return;
//        }
//    }

//    public class Solution
//    {
//        public static void Main()
//        {
//            var notesStoreObj = new NotesStore();
//            var n = int.Parse(Console.ReadLine());
//            for (var i = 0; i < n; i++)
//            {
//                var operationInfo = Console.ReadLine().Split(' ');
//                try
//                {
//                    if (operationInfo[0] == "AddNote")
//                        notesStoreObj.AddNote(operationInfo[1], operationInfo.Length == 2 ? "" : operationInfo[2]);
//                    else if (operationInfo[0] == "GetNotes")
//                    {
//                        var result = notesStoreObj.GetNotes(operationInfo[1]);
//                        if (result.Count == 0)
//                            Console.WriteLine("No Notes");
//                        else
//                            Console.WriteLine(string.Join(",", result));
//                    }
//                    else
//                    {
//                        Console.WriteLine("Invalid Parameter");
//                    }
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine("Error: " + e.Message);
//                }
//            }
//        }
//    }
//}