﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

/*
    Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.

    Consider the number of elements in nums which are not equal to val be k, to get accepted, you need to do the following things:

    Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
    Return k.
    Custom Judge:

    The judge will test your solution with the following code:
 */

using System;

namespace practice;

class Program
{
    static void Main(string[] args)
    {
        //Node node = new Node("Hello");
        //Node node2 = new Node("World");
        Node.CreateNode("Hello");
        Node.CreateNode("World");
        LinkedListEx.UpdateLinkedList();
        LinkedListEx.UpdateLinkedList();
    }
}