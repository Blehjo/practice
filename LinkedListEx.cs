﻿using System;
namespace practice;

public class LinkedListEx
{
    public static Node? Head { get; set; }
    public static Node? Tail { get; set; }

    public static void UpdateLinkedList()
    {
        if (Tail.Value != null)
        Console.WriteLine(Tail.Value);
    }
}

public class Node
{
    public string Value { get; set; }
    public Node? Next { get; set; } 

    public Node(string value)
    {
        Value = value;
    }

    // Create a node and insert it in the beginning
    public static void CreateNode(string word)
    {
        Node node = new Node(word);
        LinkedListEx.Head = node;
        //LinkedListEx.UpdateLinkedList();
    }

    // Create a node and insert it in the end
    public static void CreateAndUpdate(string word)
    {
        Node node = new Node(word);
        if (LinkedListEx.Head != null)
        {
            LinkedListEx.Head.Next = node;
            LinkedListEx.Tail = node;
        }
    }

    // Create a node and insert it in a specific location

    // Remove Node and update remaining nodes
}